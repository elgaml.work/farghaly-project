const DishesData = [
    {
        key: 1,
        titleEn: "dish 1",
        titleAr: "قصب",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fistockphoto-1278852966-612x612.jpg?alt=media&token=bb9214c0-8af5-4e24-9ad1-1309ac20c745",
        price:  [{price: 10, type: "وسط"},{price: 15, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 2,
        titleEn: "dish 1",
        titleAr: "قصب جنزبيل",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fistockphoto-1147552400-612x612.jpg?alt=media&token=4e9db17d-5b7b-43c8-b2f4-1af302aa17c1",
        price:  [{price: 15, type: "وسط"},{price: 20, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 3,
        titleEn: "dish 1",
        titleAr: "قصب باللبن",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fistockphoto-1367782625-612x612.jpg?alt=media&token=108ad419-dd70-40ad-b35a-de3830fcb819",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 4,
        titleEn: "dish 1",
        titleAr: "قصب برتقال",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fistockphoto-636628456-612x612.jpg?alt=media&token=2cdf077e-794d-4b27-befb-2bcf3816f052",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 5,
        titleEn: "dish 1",
        titleAr: "قصب بالايس",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fistockphoto-1278852966-612x612.jpg?alt=media&token=bb9214c0-8af5-4e24-9ad1-1309ac20c745",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 6,
        titleEn: "dish 1",
        titleAr: "قصب على سوبيا",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fistockphoto-1156304402-612x612.jpg?alt=media&token=41bb1310-d819-4ff0-af22-dabe37603474",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 7,
        titleEn: "dish 1",
        titleAr: "قصب ليمون",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fsugarcane-lemon-juice-for-your-immune-system.jpg?alt=media&token=063fcdd8-06f5-4f79-a04b-983aebc8e69b",
        price:  [{price: 15, type: "وسط"},{price: 20, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 8,
        titleEn: "dish 1",
        titleAr:"قصب جرجير",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fistockphoto-509117702-612x612.jpg?alt=media&token=174769a6-357d-4dfd-bac9-f2d66c708335",
        price:  [{price: 15, type: "وسط"},{price: 20, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },{
        key: 300,
        titleEn: "dish 1",
        titleAr:"قصب بالجزر",
        image: "https://images.akhbarelyom.com/images/images/large/20210117185708339.jpg",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 9,
        titleEn: "dish 1",
        titleAr:"تمر",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F15mJFR2.2.2020.jpg?alt=media&token=1c7e17fb-5f0a-42c0-9b16-94b031ee8edf",
        price:  [{price: 15, type: "وسط"},{price: 20, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 10,
        titleEn: "dish 1",
        titleAr:"تمر على سوبيا",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F15mJFR2.2.2020.jpg?alt=media&token=1c7e17fb-5f0a-42c0-9b16-94b031ee8edf",
        price:  [{price: 15, type: "وسط"},{price: 20, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 11,
        titleEn: "dish 1",
        titleAr: "سوبيا",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(2).jpeg?alt=media&token=887d4e13-ea72-4642-bfab-8a237b025be1",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 12,
        titleEn: "dish 1",
        titleAr: "خروب",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F137.jpg?alt=media&token=8067c1b4-29a8-48a4-a0f1-a6ca324c3d36",
        price:  [{price: 15, type: "وسط"},{price: 20, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 13,
        titleEn: "dish 1",
        titleAr: "عناب",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FEN1712023.jpg?alt=media&token=2f3464d8-8236-4606-bd1d-a6d4b187e13c",
        price:  [{price: 15, type: "وسط"},{price: 20, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 14,
        titleEn: "dish 1",
        titleAr: "جزر",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F878551d3f20b24b661798c264b95d96a_w750_h500.jpg?alt=media&token=3c4d8841-c8c4-4e10-b386-415ba93d3000",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 15,
        titleEn: "dish 1",
        titleAr: "دوم",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%AA%D8%AD%D8%B6%D9%8A%D8%B1-%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D8%AF%D9%88%D9%85-%D8%A7%D9%84%D9%85%D8%AC%D8%B1%D9%88%D8%B4.jpg?alt=media&token=a46c2738-d95e-404f-9108-760ab990c308",
        price:  [{price: 15, type: "وسط"},{price: 20, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 16,
        titleEn: "dish 1",
        titleAr: "دوم باللبن",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fmaxresdefault.jpg?alt=media&token=ba5d266e-63db-4451-842b-3044b272b19d",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 17,
        titleEn: "dish 1",
        titleAr: "قصب بطيخ",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fmaxresdefault%20(1).jpg?alt=media&token=e9f797be-3c34-4d52-bd21-30aaf7ec7a55",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 18,
        titleEn: "dish 1",
        titleAr: "قصب حلاوة",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fsugarcane-gda2cc1189_6401-640x400.webp?alt=media&token=43c659fe-b586-4bf4-89ec-b560d8b41db4",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },
    {
        key: 19,
        titleEn: "dish 1",
        titleAr: "قصب سفن اب",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F2020_5_26_18_6_41_38.jpg?alt=media&token=7db1d286-0cfe-4743-b2d6-0de65614f6ba",
        price:  [{price: 20, type: "وسط"},{price: 25, type: "كبير"}],
        category: "oriental_juices",
        details: ""
    },{
        key: 20,
        titleEn: "dish 1",
        titleAr: "عصير ليمون",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F4mJFR333.1.2020.jpg?alt=media&token=55c61f01-634d-4082-aea2-379510541302",
        price:  30,
        category: "nutural_juices",
        details: ""
    },{
        key: 21,
        titleEn: "dish 1",
        titleAr: "عصير ليمون نعناع",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D9%84%D9%8A%D9%85%D9%88%D9%86-%D8%A8%D8%A7%D9%84%D9%86%D8%B9%D9%86%D8%A7%D8%B9-%D8%A7%D9%84%D9%85%D9%86%D8%B9%D8%B4-600x430.jpg?alt=media&token=c05cee99-fbdd-4aa0-a3f6-a8ad9ab4ad3a",
        price:  [{price: 25, type: "وسط"},{price: 30, type: "كبير"}],
        category: "nutural_juices",
        details: ""
    },{
        key: 22,
        titleEn: "dish 1",
        titleAr: "عصير ليمون باللبن",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D9%84%D9%8A%D9%85%D9%88%D9%86-%D8%A8%D8%A7%D9%84%D9%84%D8%A8%D9%86.jpg?alt=media&token=12f4f3bf-1aae-4173-9937-474a2a2e9616",
        price:  [{price: 25, type: "وسط"},{price: 30, type: "كبير"}],
        category: "nutural_juices",
        details: ""
    },{
        key: 23,
        titleEn: "dish 1",
        titleAr: "عصير فراوله",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F8edc1961e731c62e9efd6c017eed5140051bd6c9-210322102430.webp?alt=media&token=1f528684-163b-4abf-9334-3e29c2037f75",
        price:  40,
        category: "nutural_juices",
        details: ""
    },{
        key: 24,
        titleEn: "dish 1",
        titleAr: "عصير جوافة",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(3).jpeg?alt=media&token=8df6642a-341a-4675-9d99-864900f44722",
        price:  35,
        category: "nutural_juices",
        details: ""
    },{
        key: 24,
        titleEn: "dish 1",
        titleAr: "عصير جوافة بالحليب",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(3).jpeg?alt=media&token=8df6642a-341a-4675-9d99-864900f44722",
        price:  40,
        category: "nutural_juices",
        details: ""
    },{
        key: 25,
        titleEn: "dish 1",
        titleAr: "عصير برتقال",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Ftbl_articles_article_20874_64472056cb5-2b35-4913-8998-fdeeb28f3101.gif?alt=media&token=aac3fb45-a9c6-4959-bc63-4548b5795109",
        price:  [{price: 25, type: "وسط"},{price: 30, type: "كبير"}],
        category: "nutural_juices",
        details: ""
    },{
        key: 27,
        titleEn: "dish 1",
        titleAr: "عصير رمان",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Ftbl_articles_article_18781_29cf234465-bced-48eb-be88-c5330b506b3f.jpg?alt=media&token=9b767fc7-85b5-430f-bf30-093a60f19fb8",
        price:  [{price: 25, type: "وسط"},{price: 30, type: "كبير"}],
        category: "nutural_juices",
        details: ""
    },{
        key: 28,
        titleEn: "dish 1",
        titleAr: "عصير بطيخ",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D8%A8%D8%B7%D9%8A%D8%AE-%D8%A8%D8%A7%D9%84%D9%86%D8%B9%D9%86%D8%A7%D8%B9.jpg?alt=media&token=f901da73-150c-48f5-b00c-6eb5a9d57f51",
        price:  30,
        category: "nutural_juices",
        details: ""
    },{
        key: 29,
        titleEn: "dish 1",
        titleAr: "عصير بطيخ رمان حب",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F672606015fbc1e59060012e3ee5f2b4845356d76-290521161142.webp?alt=media&token=1debc00a-c6f6-491b-8c86-7e492a1e92f9",
        price:  [{price: 25, type: "وسط"},{price: 30, type: "كبير"}],
        category: "nutural_juices",
        details: ""
    },{
        key: 30,
        titleEn: "dish 1",
        titleAr: "عصير بطيخ نعناع",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B9%D9%85%D9%84-%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D8%A8%D8%B7%D9%8A%D8%AE-%D8%A8%D8%A7%D9%84%D9%86%D8%B9%D9%86%D8%A7%D8%B9-%D8%A7%D9%84%D9%85%D9%86%D8%B9%D8%B4-%D8%A8%D8%A7%D9%84%D8%B5%D9%88%D8%B113.jpg?alt=media&token=02c477ec-5f3d-4462-baea-557831609a98",
        price:  40,
        category: "nutural_juices",
        details: ""
    },{
        key: 31,
        titleEn: "dish 1",
        titleAr: "عصير عنب",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(4).jpeg?alt=media&token=7a1fee37-13a4-4804-b170-7f54ef1c75c7",
        price:  40,
        category: "nutural_juices",
        details: ""
    },{
        key: 32,
        titleEn: "dish 1",
        titleAr: "عصير خوخ",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B9%D8%B5%D9%8A%D8%B1-%D8%AE%D9%88%D8%AE.jpg?alt=media&token=86c4518e-e82d-43b7-a0d8-9e7b4e0bdfd2",
        price:  40,
        category: "nutural_juices",
        details: ""
    },{
        key: 33,
        titleEn: "dish 1",
        titleAr: "عصير برقوق",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Ftbl_articles_article_26890_7269ad7575d-1ff7-40ca-a469-3a0d4085d6d6.jpg?alt=media&token=588a1d93-11d6-4476-8103-0e7eff80cd70",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "nutural_juices",
        details: ""
    },{
        key: 35,
        titleEn: "dish 1",
        titleAr: "عصير كنتالوب",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Ff71f0dc6d45159076525338f12e80e1b.webp?alt=media&token=45b58753-7100-42b4-8bf6-92b626bf1dfa",
        price:  35,
        category: "nutural_juices",
        details: ""
    },{
        key: 37,
        titleEn: "dish 1",
        titleAr: "عصير موز باللبن",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D9%83%D9%8A%D9%81_%D8%A7%D8%B3%D9%88%D9%8A_%D8%B9%D8%B5%D9%8A%D8%B1_%D9%85%D9%88%D8%B2.jpg?alt=media&token=be54f782-a5b9-45bd-a248-b2e719758baf",
        price:  35,
        category: "nutural_juices",
        details: ""
    },{
        key: 38,
        titleEn: "dish 1",
        titleAr: "عصير بلح باللبن",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D8%A8%D9%84%D8%AD-4-750x430.jpg?alt=media&token=838a06c0-487f-4e24-a686-b74b956d9926",
        price:  [{price: 30, type: "وسط"},{price: 35, type: "كبير"}],
        category: "nutural_juices",
        details: ""
    },{
        key: 39,
        titleEn: "dish 1",
        titleAr: "عصير تفاح",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fd2f871b13f550f3ebb365820d99e2ba3a171c809.jpg?alt=media&token=085d04ea-a67e-432f-ab70-b9dd13b1e44e",
        price:  40,
        category: "nutural_juices",
        details: ""
    },{
        key: 40,
        titleEn: "dish 1", 
        titleAr: "عصير اناناس",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(5).jpeg?alt=media&token=d70099cf-5e80-48bd-a9a9-4c53f866d8d6",
        price:  55,
        category: "nutural_juices",
        details: ""
    },{
        key: 41,
        titleEn: "dish 1",
        titleAr: "عصير كيوي",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F13mJFR2.2.2020.jpg?alt=media&token=fc122992-454a-4133-8129-7b5e9a2b9e1b",
        price:  50,
        category: "nutural_juices",
        details: ""
    },{
        key: 42,
        titleEn: "dish 1",
        titleAr: "مانجو",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D9%85%D8%A7%D9%86%D8%AC%D9%88.jpg?alt=media&token=f7782925-15ea-4f1b-b3d0-2a50ed314403",
        price:  40,
        category: "mango_world",
        details: ""
    },{
        key: 43,
        titleEn: "dish 1",
        titleAr: "مانجو + فراوله",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(6).jpeg?alt=media&token=77392cbf-d219-490d-9287-81adf657ca8e",
        price:  45,
        category: "mango_world",
        details: ""
    },{
        key: 44,
        titleEn: "dish 1",
        titleAr: "مانجو + خوخ",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Flazezh_1735716a97.jpg?alt=media&token=fb892203-e376-4f8d-a5d5-e68cd94cb0e3",
        price: 45,
        category: "mango_world",
        details: ""
    },{
        key: 45,
        titleEn: "dish 1",
        titleAr: "مانجو + موز",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F7913dcfdf6d270d70d28c42b00a044b6155882a1.jpg?alt=media&token=fd0ed586-253c-4d93-9299-2607b6dd0d4c",
        price:  45,
        category: "mango_world",
        details: ""
    },{
        key: 46,
        titleEn: "dish 1",
        titleAr: "مانجو + اناناس",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B3%D9%85%D9%88%D8%AB%D9%8A-%D8%A7%D9%84%D8%A3%D9%86%D8%A7%D9%86%D8%A7%D8%B3-%D9%88%D8%A7%D9%84%D9%85%D8%A7%D9%86%D8%AC%D9%88.jpg?alt=media&token=0b0abb3e-7019-49cb-a2a8-dc3f992e00a6",
        price:  45,
        category: "mango_world",
        details: ""
    },{
        key: 47,
        titleEn: "dish 1",
        titleAr: "مانجو + كيوى",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Flazezh_cf488cca08.jpg?alt=media&token=69dcb969-49cc-4d89-8b55-3c42df857472",
        price:  45,
        category: "mango_world",
        details: ""
    },{
        key: 48,
        titleEn: "dish 1",
        titleAr: "مانجو + تفاح",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B9%D8%B5%D9%8A%D8%B1-%D9%85%D8%A7%D9%86%D8%AC%D9%88-%D8%A8%D8%A7%D9%84%D9%86%D8%B9%D9%86%D8%A7%D8%B9.jpg?alt=media&token=af087d85-7cd5-4c8d-b500-6b623117c0ad",
        price: 45,
        category: "mango_world",
        details: ""
    },{
        key: 49,
        titleEn: "dish 1",
        titleAr: "مانجو + لبن",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fphoto.jpg?alt=media&token=4ae87a07-988f-4981-b2c9-67cce9839e3b",
        price:  45,
        category: "mango_world",
        details: ""
    },{
        key: 50,
        titleEn: "dish 1",
        titleAr: "مانجو + بطيخ",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fphoto%20(1).jpg?alt=media&token=17357cba-c621-44a8-8b81-041db2f11821",
        price: 45,
        category: "mango_world",
        details: ""
    },{
        key: 51,
        titleEn: "dish 1",
        titleAr: "مانجو + ايس",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F916627a6fee42c5d4e5566807cd168e7dd3d67bd.jpg?alt=media&token=76ecf2e8-007b-49da-90fd-52979bfc968b",
        price:  45,
        category: "mango_world",
        details: ""
    },{
        key: 52,
        titleEn: "dish 1",
        titleAr: "مانجو + حب رمان",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FMANGO_JUSTFOOD.jpg?alt=media&token=11df253a-a27c-43aa-9256-8ea424804c20",
        price:  45,
        category: "mango_world",
        details: ""
    },{
        key: 53,
        titleEn: "dish 1",
        titleAr: "مانجو + سوبيا",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(7).jpeg?alt=media&token=28e3c9d1-c6fa-4391-b3c9-fb9c1280e217",
        price: 45,
        category: "mango_world",
        details: ""
    },{
        key: 54,
        titleEn: "dish 1",
        titleAr: "أوريو",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(8).jpeg?alt=media&token=3ca8092a-5e05-4a5a-9ff7-b57805280dbe",
        price:  [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 55,
        titleEn: "dish 1",
        titleAr: "بوريو",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B7%D8%B1%D9%8A%D9%82%D8%A9_%D8%B9%D9%85%D9%84_%D9%85%D9%8A%D9%84%D9%83_%D8%B4%D9%8A%D9%83_%D8%A3%D9%88%D8%B1%D9%8A%D9%88.jpg?alt=media&token=e0e6858d-a33c-4596-b101-920e7158d9ce",
        price:  [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 56,
        titleEn: "dish 1",
        titleAr: "هوهوز",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fmaxresdefault%20(2).jpg?alt=media&token=522c9766-9054-4cb1-8c28-473c6b35dd6c",
        price:  [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 57,
        titleEn: "dish 1",
        titleAr: "توينكز",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(9).jpeg?alt=media&token=3bc82b3a-605f-4ed2-acde-9990fbb24d66",
        price:  [{price: 30, type: "وسط"},{price: 35, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 58,
        titleEn: "dish 1",
        titleAr: "براونيز",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fmaxresdefault%20(3).jpg?alt=media&token=83b0b601-64f4-4c9a-a3a1-5156feaa0469",
        price:  [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 59,
        titleEn: "dish 1",
        titleAr: "مالتيزرز",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(10).jpeg?alt=media&token=ca2bfe29-d4ac-4c88-9324-0aca16580ce2",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 60,
        titleEn: "dish 1",
        titleAr: "كيندر",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(11).jpeg?alt=media&token=5169bb61-c7c4-4dc1-b2e5-79119b31a14b",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 61,
        titleEn: "dish 1",
        titleAr: "جلاكسي",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(12).jpeg?alt=media&token=12772da4-0b24-4de7-8fdd-b32c2827d4fd",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 62,
        titleEn: "dish 1",
        titleAr: "كادبورى",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F70057-%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%A7%D9%84%D9%85%D9%8A%D9%84%D9%83-%D8%B4%D9%8A%D9%836.jpg?alt=media&token=beb83a73-1dab-4c79-826f-6fc142229d42",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 63,
        titleEn: "dish 1",
        titleAr: "كيت كات",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FFIRUZE-202-HD.jpg?alt=media&token=001b1374-550b-45e0-aa73-f7a647a362d9",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 64,
        titleEn: "dish 1",
        titleAr: "ميجا",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B7%D8%B1%D9%82-%D8%B9%D9%85%D9%84-%D9%85%D9%8A%D9%84%D9%83-%D8%B4%D9%8A%D9%83.webp?alt=media&token=b0082589-a2f0-42a6-8220-1e198e9ecf5a",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 65,
        titleEn: "dish 1",
        titleAr: "سنيكرز",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fmqdefault.jpg?alt=media&token=b8d813ad-a8eb-4719-bd9c-6aa33a700558",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 66,
        titleEn: "dish 1",
        titleAr: "مارس",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F70057-%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%A7%D9%84%D9%85%D9%8A%D9%84%D9%83-%D8%B4%D9%8A%D9%836%20(1).jpg?alt=media&token=e2de8d58-ff61-4729-93dd-d702a603e48a",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 67,
        titleEn: "dish 1",
        titleAr: "تويكس",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FFIRUZE-207-HD.jpg?alt=media&token=bec7db82-9e1e-4c12-bc6b-7c1603178736",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 68,
        titleEn: "dish 1",
        titleAr: "فلوتس",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D9%85%D9%8A%D9%84%D9%83-%D8%B4%D9%8A%D9%83-%D8%A7%D9%84%D8%B4%D9%88%D9%83%D9%88%D9%84%D8%A7%D8%AA%D8%A9-%D8%A8%D8%A3%D8%B7%D9%8A%D8%A8-%D9%85%D8%B0%D8%A7%D9%82.jpeg?alt=media&token=4e087efa-615d-4103-89a4-612b482d5ac4",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 69,
        titleEn: "dish 1",
        titleAr: "نوتيلا",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D9%85%D9%8A%D9%84%D9%83-%D8%B4%D9%8A%D9%83-%D9%86%D9%88%D8%AA%D9%8A%D9%84%D8%A7.jpg?alt=media&token=b49296fc-de9c-40cb-8264-8e58fa4dbcf1",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 70,
        titleEn: "dish 1",
        titleAr: "باونتي",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FKINDER-MILK-SHAKE.jpg?alt=media&token=59519ceb-c0ea-4d7d-b342-718c06bc893b",
        price:  [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 71,
        titleEn: "dish 1",
        titleAr: "فيريرو روشيه",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fphoto%20(2).jpg?alt=media&token=e2572a63-7c72-459e-b4ba-ec3c917e8a64",
        price:  [{price: 50, type: "وسط"},{price: 55, type: "كبير"}],
        category: "choco_world",
        details: ""
    },{
        key: 72,
        titleEn: "dish 1",
        titleAr: "1 بوله",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(13).jpeg?alt=media&token=70e81c3f-54bc-4c00-9b06-493027fcd627",
        price:  15,
        category: "ice_cream",
        details: ""
    },{
        key: 73,
        titleEn: "dish 1",
        titleAr: "2 بوله",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(13).jpeg?alt=media&token=70e81c3f-54bc-4c00-9b06-493027fcd627",
        price:  30,
        category: "ice_cream",
        details: ""
    },{
        key: 74,
        titleEn: "dish 1",
        titleAr: "3 بوله",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(13).jpeg?alt=media&token=70e81c3f-54bc-4c00-9b06-493027fcd627",
        price:  40,
        category: "ice_cream",
        details: ""
    },{
        key: 75,
        titleEn: "dish 1",
        titleAr: "الاسد",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F201905170127562756%20(1).jpg?alt=media&token=0d869e9b-b3b2-4456-b78e-d9a538594acc",
        price:   [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "jungle_feedings",
        details: "سوبيا - مانجو - ايس"
    },{
        key: 76,
        titleEn: "dish 1",
        titleAr: "القرد",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F201905170127562756%20(1).jpg?alt=media&token=0d869e9b-b3b2-4456-b78e-d9a538594acc",
        price:   [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "jungle_feedings",
        details: "سوبيا - مانجو - موز"
    },{
        key: 77,
        titleEn: "dish 1",
        titleAr: "الفيل",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F201905170127562756%20(1).jpg?alt=media&token=0d869e9b-b3b2-4456-b78e-d9a538594acc",
        price:   [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "jungle_feedings",
        details: "سوبيا - شيكولاته - ايس"
    },{
        key: 78,
        titleEn: "dish 1",
        titleAr: "النمر",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F201905170127562756%20(1).jpg?alt=media&token=0d869e9b-b3b2-4456-b78e-d9a538594acc",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "jungle_feedings",
        details: "سوبيا - فراولة - ايس"
    },{
        key: 79,
        titleEn: "dish 1",
        titleAr: "الحوت",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F201905170127562756%20(1).jpg?alt=media&token=0d869e9b-b3b2-4456-b78e-d9a538594acc",
        price:   [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "jungle_feedings",
        details: "سوبيا - كنتالوب - ايس"
    },{
        key: 80,
        titleEn: "dish 1",
        titleAr: "الغزال",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F201905170127562756%20(1).jpg?alt=media&token=0d869e9b-b3b2-4456-b78e-d9a538594acc",
        price:   [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "jungle_feedings",
        details: "سوبيا - بلح - ايس"
    },{
        key: 81,
        titleEn: "dish 1",
        titleAr: "سموزى مانجو",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FMango-Avocado-Smoothie.jpg?alt=media&token=c5e82a92-03e2-40f6-b91b-fb1b13f292c3",
        price:  45,
        category: "farghly_smoothie",
        details: ""
    },{
        key: 82,
        titleEn: "dish 1",
        titleAr: "سموزى خوخ",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F20221011194140916.jpg?alt=media&token=c26c271b-4fcc-4fe3-9e57-b7d1d49d0b52",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghly_smoothie",
        details: ""
    },{
        key: 83,
        titleEn: "dish 1",
        titleAr: "سموزى فراوله",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B3%D9%85%D9%88%D8%B2%D9%8A-%D8%A7%D9%84%D9%81%D8%B1%D8%A7%D9%88%D9%84%D8%A9-%D8%A8%D8%A7%D9%84%D8%B2%D8%A8%D8%A7%D8%AF%D9%8A.jpg?alt=media&token=8990066a-72bd-4829-9f91-99367ee4b36f",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghly_smoothie",
        details: ""
    },{
        key: 84,
        titleEn: "dish 1",
        titleAr: "سموزى بطيخ",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FWatermelon_Smoothie.jpg?alt=media&token=1d033ae5-6e2b-43f0-ba4c-81c1645830d1",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghly_smoothie",
        details: ""
    },{
        key: 85,
        titleEn: "dish 1",
        titleAr: "سموزى ليمون",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F21467339631594147768.jpg?alt=media&token=1c649703-3c01-401d-a625-5c4dfa57a159",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghly_smoothie",
        details: ""
    },{
        key: 86,
        titleEn: "dish 1",
        titleAr: "سموزى توت برى",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%B3%D9%85%D9%88%D8%AB%D9%8A-%D8%A7%D9%84%D8%AA%D9%88%D8%AA.jpg?alt=media&token=37a5941d-db8a-4e2a-822f-469c3e1ffa20",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghly_smoothie",
        details: ""
    },{
        key: 87,
        titleEn: "dish 1",
        titleAr: "سموزى بلو بيرى",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F0e18ab593e0f00da582968c529f101d9e11d0ea9-250822080723.webp?alt=media&token=25ef2a36-d7fd-4323-b9af-a6b925f12de1",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghly_smoothie",
        details: ""
    },{
        key: 88,
        titleEn: "dish 1",
        titleAr: "سموزى حسب اختيارك",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F2016_8_11_15_35_46_589.jpg?alt=media&token=ab15af27-fd8f-41be-8872-8ba1ed8268a1",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghly_smoothie",
        details: ""
    },{
        key: 89,
        titleEn: "dish 1",
        titleAr: "ميلك شيك فانيليا",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F12301141731562942515.jpg?alt=media&token=2d1cb49f-41f0-4124-afa6-a8845b22a137",
        price:  40,
        category: "milkshake",
        details: ""
    },{
        key: 90,
        titleEn: "dish 1",
        titleAr: "ميلك شيك شيكولاته",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F7570837531582895388.jpg?alt=media&token=dbd37d50-83b2-43f8-8bae-1d9cc8d11d95",
        price:  40,
        category: "milkshake",
        details: ""
    },{
        key: 91,
        titleEn: "dish 1",
        titleAr: "ميلك شيك فراوله",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fstrawberry%20milkshake.jpg?alt=media&token=cffacb17-3462-4297-b0b0-54573e2e64a0",
        price:  40,
        category: "milkshake",
        details: ""
    },{
        key: 92,
        titleEn: "dish 1",
        titleAr: "ميلك شيك مانجو",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D9%85%D9%8A%D9%84%D9%83-%D8%B4%D9%8A%D9%83-%D8%A7%D9%84%D9%85%D8%A7%D9%86%D8%AC%D9%88.jpg?alt=media&token=debe9909-10ff-4952-874f-f6a6abf79361",
        price:  40,
        category: "milkshake",
        details: ""
    },{
        key: 93,
        titleEn: "dish 1",
        titleAr: "ميلك شيك موكا",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F3757796-1217922229.jpg?alt=media&token=b5950245-8b64-4a0b-bfb4-49f504d99464",
        price:  40,
        category: "milkshake",
        details: ""
    },{
        key: 94,
        titleEn: "dish 1",
        titleAr: "ميلك شيك اوريو",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fwsi-imageoptim-%D8%A7%D9%88%D8%B1%D9%8A%D9%88.jpg?alt=media&token=f5cfb48e-0827-42b3-9dec-88d9373eda6c",
        price:  40,
        category: "milkshake",
        details: ""
    },{
        key: 95,
        titleEn: "dish 1",
        titleAr: "ميلك شيك بوريو",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(14).jpeg?alt=media&token=95c98af6-32d0-4b93-a8da-a9f1caf2a118",
        price:  40,
        category: "milkshake",
        details: ""
    },{
        key: 96,
        titleEn: "dish 1",
        titleAr: "ميلك شيك كراميل",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(15).jpeg?alt=media&token=4c0e7cd3-abf4-49b6-b2c2-6461f3982919",
        price:  40,
        category: "milkshake",
        details: ""
    },{
        key: 97,
        titleEn: "dish 1",
        titleAr: "ميلك شيك توت برى احمر",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F18374-milkshakes-cranberries.jpg?alt=media&token=ad2dc244-952d-4808-9ba5-d4a2a2ebcd4a",
        price: 40,
        category: "milkshake",
        details: ""
    },{
        key: 98,
        titleEn: "dish 1",
        titleAr: "ميلك شيك بلو بيرى",
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D9%85%D9%8A%D9%84%D9%83-%D8%B4%D9%8A%D9%83-%D8%A8%D9%84%D9%88%D8%A8%D9%8A%D8%B1%D9%8A.jpg?alt=media&token=053e2251-2512-4dcf-87cf-b42f32a4a0bb",
        price:  40,
        category: "milkshake",
        details: ""
    },{
        key: 99,
        titleEn: "dish 1",
        titleAr: "زبادوو ساده",
        image: "https://www.lazezh.com/wp-content/uploads/gcMigration/recipes/a/a34febecd33e904-670x350.jpg",
        price: 40,
        category: "zabado",
        details: ""
    },{
        key: 100,
        titleEn: "dish 1",
        titleAr: "زبادو بالعسل",
        image: "https://shamlola.s3.amazonaws.com/Shamlola_Images/9/src/48628535dd2d233e1ea0278af0aaefc1dc6683e7.jpg",
        price: 40,
        category: "zabado",
        details: ""
    },{
        key: 101,
        titleEn: "dish 1",
        titleAr: "زبادو فواكه عصر",
        image: "https://www.elbalad.news/UploadCache/libfiles/827/6/600x338o/339.jpg",
        price: 40,
        category: "zabado",
        details: ""
    },{
        key: 102,
        titleEn: "dish 1",
        titleAr: "زبادو حسب اختيارك",
        image: "https://pbs.twimg.com/media/Ei94BT_XYAUhAqX.jpg",
        price: 40,
        category: "zabado",
        details: ""
    },{
        key: 103,
        titleEn: "dish 1",
        titleAr: "زبادو بندق فانيليا",
        image: "https://elwasfa.com/wp-content/uploads/2017/09/IMG_1975-2.jpg",
        price: 40,
        category: "zabado",
        details: ""
    },{
        key: 104,
        titleEn: "dish 1",
        titleAr: "زبادو توت",
        image: "https://shamlola.s3.amazonaws.com/Shamlola_Images/7/src/583b99f80c080148042685198af89448dd0217df.jpg",
        price: 40,
        category: "zabado",
        details: ""
    },{
        key: 105,
        titleEn: "dish 1",
        titleAr: "زبادو كريز",
        image: "https://www.sally-fouad.com/wp-content/uploads/2017/08/FB_IMG_1503110912107.jpg",
        price: 40,
        category: "zabado",
        details: ""
    },{
        key: 106,
        titleEn: "dish 1",
        titleAr: "زبادو مانجو",
        image: "https://static.aljamila.com/styles/1100x732/public/lazezh_0ae16d47df.jpg?h=bd447cc8",
        price: 40,
        category: "zabado",
        details: ""
    },{
        key: 107,
        titleEn: "dish 1",
        titleAr: "زبادو خوخ",
        image: "https://www.justfood.tv/big/0Peach.Smoothie.jpg",
        price: 40,
        category: "zabado",
        details: ""
    },{
        key: 108,
        titleEn: "dish 1",
        titleAr: "زبادو بطيخ",
        image: "https://www.lazezh.com/wp-content/uploads/gcMigration/recipes/l/lazezh_bff1fbddd1.jpg",
        price: 40,
        category: "zabado",
        details: ""
    },{
        key: 109,
        titleEn: "dish 1",
        titleAr: "سودانى شيكولاته",
        image: "https://snacks-roastery.com/wp-content/uploads/2022/10/%D8%B3%D9%88%D8%AF%D8%A7%D9%86%D9%8A-%D8%A8%D8%A7%D9%84%D8%B4%D9%88%D9%83%D9%88%D9%84%D8%A7.png",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghaly_nuts",
        details: ""
    },{
        key: 110,
        titleEn: "dish 1",
        titleAr: "سودانى فانيليا",
        image: "https://i0.wp.com/azharspices.com/wp-content/uploads/2020/09/%D8%B3%D9%88%D8%AF%D8%A7%D9%86%D9%89-%D9%85%D9%82%D8%B4%D8%B1-%D9%86%D9%89.png?fit=700%2C700&ssl=1",
        price:  [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghaly_nuts",
        details: ""
    },{
        key: 111,
        titleEn: "dish 1",
        titleAr: "بندق شيكولاته",
        image: "https://aklat.net/foods/269/14170-balls-with-hazelnut-chocolate.jpg",
        price:  [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_nuts",
        details: ""
    },{
        key: 112,
        titleEn: "dish 1",
        titleAr: "بندق فانيليا",
        image: "https://cdn.alweb.com/thumbs/sahhawhana/article/fit630x300/%D9%83%D9%8A%D9%81%D9%8A%D8%A9_%D8%AA%D8%AD%D9%85%D9%8A%D8%B5_%D8%A7%D9%84%D8%A8%D9%86%D8%AF%D9%82.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_nuts",
        details: ""
    },{
        key: 113,
        titleEn: "dish 1",
        titleAr: "كاجو فانيليا",
        image: "https://vid.alarabiya.net/images/2021/08/21/c303cadd-7af3-4b99-8d8a-75cc42fb1de9/c303cadd-7af3-4b99-8d8a-75cc42fb1de9.jpg?crop=4:3&width=1200",
        price: [{price: 60, type: "وسط"},{price: 65, type: "كبير"}],
        category: "farghaly_nuts",
        details: ""
    },{
        key: 114,
        titleEn: "dish 1",
        titleAr: "فسدق فانيليا",
        image: "https://img.youm7.com/large/920143122042.jpg",
        price: [{price: 60, type: "وسط"},{price: 65, type: "كبير"}],
        category: "farghaly_nuts",
        details: ""
    },{
        key: 115,
        titleEn: "dish 1",
        titleAr: "لوز شيكولاته",
        image: "https://static.webteb.net/images/content/tbl_articles_article_14061_87.jpg",
        price: [{price: 60, type: "وسط"},{price: 65, type: "كبير"}],
        category: "farghaly_nuts",
        details: ""
    },{
        key: 116,
        titleEn: "dish 1",
        titleAr: "لوز فانيليا",
        image: "https://static.webteb.net/images/content/tbl_articles_article_14061_87.jpg",
        price: [{price: 60, type: "وسط"},{price: 65, type: "كبير"}],
        category: "farghaly_nuts",
        details: ""
    },{
        key: 117,
        titleEn: "dish 1",
        titleAr: "مكس",
        image: "https://belbalady.net/temp/thumb/900x450_uploads,2023,04,09,f381f18b19.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "بلح - موز - مانجو - تفاح - لبن"
    },{
        key: 118,
        titleEn: "dish 1",
        titleAr: "فرافير",
        image: "https://media.gemini.media/img/large/2018/5/25/2018_5_25_18_27_3_397.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "مانجو - موز - لبن - ايس"
    },{
        key: 119,
        titleEn: "dish 1",
        titleAr: "بطوط",
        image: "https://static.aljamila.com/styles/1100x732/public/lazezh_0ae16d47df.jpg?h=bd447cc8",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "مانجو - موز - لبن - ايس"
    },{
        key: 120,
        titleEn: "dish 1",
        titleAr: "كثافه",
        image: "https://www.supermama.me/system/App/Models/Recipe/images/000/107/360/watermarked/%D8%B9%D8%B5%D9%8A%D8%B1-%D9%85%D8%A7%D9%86%D8%AC%D9%88-%D8%A8%D8%A7%D9%84%D8%A7%D9%8A%D8%B3-%D9%83%D8%B1%D9%8A%D9%85.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "مانجو - موز - ايس"
    },{
        key: 121,
        titleEn: "dish 1",
        titleAr: "انتعاش",
        image: "https://www.supermama.me/system/App/Models/Recipe/images/000/098/793/watermarked/%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D9%83%D9%8A%D9%88%D9%8A-%D9%88%D8%A7%D9%84%D8%AA%D9%81%D8%A7%D8%AD-%D8%A7%D9%84%D8%A3%D8%AE%D8%B6%D8%B1.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "تفاح - ليمون - نعناع"
    },{
        key: 122,
        titleEn: "dish 1",
        titleAr: "الاصفهانى",
        image: "https://img-global.cpcdn.com/recipes/0b7da578f0f79bf4/1200x630cq70/photo.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "مانجو - زبادى - لبن - ايس"
    },{
        key: 123,
        titleEn: "dish 1",
        titleAr: "فيتامين سي",
        image: "https://images.akhbarelyom.com/images/images/large/20210329110007968.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "جوافه - برتفال - ليمون - عسل"
    },{
        key: 124,
        titleEn: "dish 1",
        titleAr: "جوجل",
        image: "https://yummy.awicdn.com/site-images/sites/default/files/prod/recipe/4/f/565794/92ed1b3e43df9d9435bda888f315d29b7ad40356-020323112842.jpg?preset=v3.0_1200xDYN&save-png=1&rnd=1519151RND220215",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "مانجو - اناناس - لبن - ايس"
    },{
        key: 125,
        titleEn: "dish 1",
        titleAr: "صافانا",
        image: "https://www.thaqafnafsak.com/wp-content/uploads/2015/08/%D8%B9%D9%85%D9%84-%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D8%A3%D9%86%D8%A7%D9%86%D8%A7%D8%B3-%D9%88%D8%A7%D9%84%D8%A8%D8%B1%D8%AA%D9%82%D8%A7%D9%84-%D8%A7%D9%84%D9%85%D9%81%D9%8A%D8%AF%D8%A9-%D9%84%D8%B5%D8%AD%D8%AA%D9%83-.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "اناناس - برتقال - تفاح"
    },{
        key: 126,
        titleEn: "dish 1",
        titleAr: "هيرو",
        image: "https://www.supermama.me/system/App/Models/Recipe/images/000/102/427/watermarked/%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D9%83%D9%88%D9%83%D8%AA%D9%8A%D9%84-%D8%A7%D9%84%D9%85%D9%86%D8%B9%D8%B4.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "اناناس - خوخ - برتقال"
    },{
        key: 127,
        titleEn: "dish 1",
        titleAr: "ابو تريكه",
        image: "https://yummy.awicdn.com/site-images/sites/default/files/prod/video/b/a/428914/0e35a65ed82724e08926c2d8ab329a985e505ea0-061121154559.jpg?preset=v3.0_1200xDYN&save-png=1&rnd=1519151RND220215",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "اناناس - موز - فراوله - ايس"
    },{
        key: 128,
        titleEn: "dish 1",
        titleAr: "الشبح",
        image: "https://cdn.alweb.com/thumbs/hotcoldcups/article/fit727x484/1/%D9%83%D9%88%D9%83%D8%AA%D9%8A%D9%84-%D8%A7%D9%84%D9%83%D9%8A%D9%88%D9%8A-%D9%88%D8%A7%D9%84%D8%AA%D9%81%D8%A7%D8%AD-%D9%85%D9%86%D8%B9%D8%B4-%D9%88%D9%84%D8%B0%D9%8A%D8%B0.jpeg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "زبادى - كيوى - تفاح"
    },{
        key: 129,
        titleEn: "dish 1",
        titleAr: "سيمبا",
        image: "https://i.ytimg.com/vi/dVwaA5--p_I/maxresdefault.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "بوريو- بلح - لبن- ايس"
    },{
        key: 130,
        titleEn: "dish 1",
        titleAr: "اكسترا",
        image: "https://yummy.awicdn.com/site-images/sites/default/files/prod/recipe/4/f/565794/92ed1b3e43df9d9435bda888f315d29b7ad40356-020323112842.jpg?preset=v3.0_1200xDYN&save-png=1&rnd=1519151RND220215",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "اناناس - مانجو - جوز هند"
    },{
        key: 131,
        titleEn: "dish 1",
        titleAr: "فيفا",
        image: "https://images.akhbarelyom.com/images/images/large/20210117203701745.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "بلح - موز - ايس"
    },{
        key: 132,
        titleEn: "dish 1",
        titleAr: "الخد الجميل",
        image: "https://www.justfood.tv/big/0Peach.Smoothie.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_mix",
        details: "برقوق - زبادى - موز - ايس"
    },
    // {
    //     key: 133,
    //     titleEn: "dish 1",
    //     titleAr: "صاروخ",
    //     image: "",
    //     price: 30,
    //     category: "farghaly_mix",
    //     details: "اناناس - موز - مانجو - ايس"
    // },{
    //     key: 134,
    //     titleEn: "dish 1",
    //     titleAr: "طاجن نوتيلا",
    //     image: "",
    //     price: 40,
    //     category: "nutella_waffel",
    //     details: ""
    // },{
    //     key: 135,
    //     titleEn: "dish 1",
    //     titleAr: "طاجن نوتيلا ايس",
    //     image: "",
    //     price: 50,
    //     category: "nutella_waffel",
    //     details: ""
    // },{
    //     key: 136,
    //     titleEn: "dish 1",
    //     titleAr: "طاجن نوتيلا فاكهه",
    //     image: "",
    //     price: 50,
    //     category: "nutella_waffel",
    //     details: ""
    // },{
    //     key: 137,
    //     titleEn: "dish 1",
    //     titleAr: "طاجن نوتيلا لوتس",
    //     image: "",
    //     price: 40,
    //     category: "nutella_waffel",
    //     details: ""
    // },{
    //     key: 138,
    //     titleEn: "dish 1",
    //     titleAr: "وافل نوتيلا",
    //     image: "",
    //     price: 30,
    //     category: "nutella_waffel",
    //     details: ""
    // },{
    //     key: 139,
    //     titleEn: "dish 1",
    //     titleAr: "وافل فاكهه",
    //     image: "",
    //     price: 40,
    //     category: "nutella_waffel",
    //     details: ""
    // },{
    //     key: 140,
    //     titleEn: "dish 1",
    //     titleAr: "وافل ايس",
    //     image: "",
    //     price: 35,
    //     category: "nutella_waffel",
    //     details: ""
    // },{
    //     key: 141,
    //     titleEn: "dish 1",
    //     titleAr: "مولتن كيك",
    //     image: "",
    //     price: 40,
    //     category: "nutella_waffel",
    //     details: ""
    // },{
    //     key: 142,
    //     titleEn: "dish 1",
    //     titleAr: "ديسباسيتو",
    //     image: "",
    //     price: 40,
    //     category: "nutella_waffel",
    //     details: ""
    // },{
    //     key: 143,
    //     titleEn: "dish 1",
    //     titleAr: "وافل لوتس",
    //     image: "",
    //     price: 35,
    //     category: "nutella_waffel",
    //     details: ""
    // },
    {
        key: 133,
        titleEn: "dish 1",
        titleAr: "افوكادو + كيوى",
        image: "https://www.supermama.me/system/App/Models/Recipe/images/000/102/005/watermarked/%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D8%A3%D9%81%D9%88%D9%83%D8%A7%D8%AF%D9%88-%D9%88%D8%A7%D9%84%D9%83%D9%8A%D9%88%D9%8A.jpg",
        price: 65,
        category: "height_power",
        details: ""
    }, {
        key: 134,
        titleEn: "dish 1",
        titleAr: "افوكادو + مانجو",
        image: "https://www.taabkh.com/files/recipe/2020/04/Mango-Avocado-Smoothie.jpg",
        price: 65,
        category: "height_power",
        details: ""
    }, {
        key: 135,
        titleEn: "dish 1",
        titleAr: "افوكادو + جنزبيل + عسل",
        image: "https://media.foodtodayeg.com/foodtoday/1076x714/3202212145620422105250.jpg",
        price: 65,
        category: "height_power",
        details: ""
    }, {
        key: 136,
        titleEn: "dish 1",
        titleAr: "افوكادو",
        image: "https://img.youm7.com/ArticleImgs/2019/8/8/65600-%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%B9%D8%B5%D9%8A%D8%B1-%D8%A7%D9%84%D8%A7%D9%81%D9%88%D9%83%D8%A7%D8%AF%D9%88.jpg",
        price: 65,
        category: "height_power",
        details: ""
    }, {
        key: 137,
        titleEn: "dish 1",
        titleAr: "افوكادو + كاجو",
        image: "https://adminhonna.elwatannews.com/uploads/image_archive/original_lower_quality/8795132641594911321.jpg",
        price: 65,
        category: "height_power",
        details: ""
    }, {
        key: 138,
        titleEn: "dish 1",
        titleAr: "افوكادو + بندق",
        image: "https://www.taabkh.com/files/recipe/2015/12/%D8%B9%D8%B5%D9%8A%D8%B1%20%D8%A7%D9%84%D8%A3%D9%81%D9%88%D9%83%D8%A7%D8%AF%D9%88%20%D9%85%D8%B9%20%D8%A7%D9%84%D8%A8%D9%86%D8%AF%D9%82.jpg",
        price: 65,
        category: "height_power",
        details: ""
    }, {
        key: 139,
        titleEn: "dish 1",
        titleAr: "افوكادو + فسدق",
        image: "https://alwafd.news/images/thumbs/828/news/2e30c394060f03b1d813550785ab1ebf.jpg",
        price: 65,
        category: "height_power",
        details: ""
    }, {
        key: 140,
        titleEn: "dish 1",
        titleAr: "افوكادو + لوز",
        image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGNalm1EQue57wrE9FMt1VeaLikQY19eCHOg&usqp=CAU",
        price: 65,
        category: "height_power",
        details: ""
    }, {
        key: 141,
        titleEn: "dish 1",
        titleAr: "افوكادو ميكس مكسرات",
        image: "https://img-global.cpcdn.com/recipes/382c02bba87f0654/1200x630cq70/photo.jpg",
        price: 65,
        category: "height_power",
        details: ""
    }, {
        key: 142,
        titleEn: "dish 1",
        titleAr: "فياجرا",
        image: "https://images.deliveryhero.io/image/talabat/Menuitems/20190418_Talabat_UAE_637473868166769236.jpg",
        price: 65,
        category: "height_power",
        details: "بلح ,جرجير ,حلاوة ,ايس ,عسل"
    },{
        key: 143,
        titleEn: "dish 1",
        titleAr: "سوبر فياجرا",
        image: "https://www.supermama.me/system/App/Models/Recipe/images/000/102/426/watermarked/%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%B9%D8%B5%D9%8A%D8%B1%20%D8%A7%D9%84%D8%A3%D9%81%D9%88%D9%83%D8%A7%D8%AF%D9%88%20%D8%A8%D8%A7%D9%84%D8%A2%D9%8A%D8%B3%20%D9%83%D8%B1%D9%8A%D9%85.jpg",
        price: 65,
        category: "height_power",
        details: "افوكادو ,جرجير ,كيوى ,حلاوة ,عسل"
    }, {
        key: 144,
        titleEn: "dish 1",
        titleAr: "لامبورجيني",
        image: "https://images.akhbarelyom.com/images/images/large/20210615093038963.jpg",
        price: 65,
        category: "height_power",
        details: "افوكادو ,جرجير ,بندق ,كاجو ,عسل"
    }, {
        key: 145,
        titleEn: "dish 1",
        titleAr: "باور فرغلى",
        image: "https://www.elbalad.news/Upload/libfiles/856/6/45.jpg",
        price: 65,
        category: "height_power",
        details: "كاجو ,فسدق ,بندق ,ايس ,حلاوة ,عنب"
    },{
        key: 146,
        titleEn: "dish 1",
        titleAr: "لبن",
        image: "https://img.youm7.com/ArticleImgs/2023/1/20/456527-%D9%84%D8%A8%D9%86.PNG",
        price: 15,
        category: "other_additions",
        details: ""
    },{
        key: 147,
        titleEn: "dish 1",
        titleAr: "عسل",
        image: "https://www.sayidaty.net/sites/default/files/styles/900_scale/public/2020/06/18/6814396-606732479.jpg",
        price: 15,
        category: "other_additions",
        details: ""
    },{
        key: 148,
        titleEn: "dish 1",
        titleAr: "ايس كريم",
        image: "https://www.atyabtabkha.com/tachyon/sites/2/2021/10/33da83ab55048285216821f47b8f3d7fd9f83337.jpg",
        price: 15,
        category: "other_additions",
        details: ""
    },{
        key: 149,
        titleEn: "dish 1",
        titleAr: "مكسرات",
        image: "https://media.gemini.media/img/large/2022/12/10/2022_12_10_12_14_35_895.jpg",
        price: 35,
        category: "other_additions",
        details: ""
    },{
        key: 150,
        titleEn: "dish 1",
        titleAr: "قشطه",
        image: "https://shamlola.s3.amazonaws.com/Shamlola_Images/8/src/f35bd20f4b3125d9f5414fa4a079742e5ba55e9c.jpg",
        price: 30,
        category: "other_additions",
        details: ""
    },{
        key: 151,
        titleEn: "dish 1",
        titleAr: "حلويات",
        image: "https://upload.wikimedia.org/wikipedia/commons/4/4b/%D8%AD%D9%84%D9%88%D9%8A%D8%A7%D8%AA_%D8%B3%D9%88%D8%B1%D9%8A%D8%A9.jpg",
        price: 30,
        category: "other_additions",
        details: ""
    },{
        key: 152,
        titleEn: "dish 1",
        titleAr: "دايت فرغلى",
        image: "https://kitchen.sayidaty.net/uploads/small/6c/6c0498c607347cb0e275b4288c39ac02_w750_h500.jpg",
        price: [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghaly_diet",
        details: "اناناس - برتقال - ليمون"
    },{
        key: 153,
        titleEn: "dish 1",
        titleAr: "دايت الرشاقه",
        image: "https://cdn.alweb.com/thumbs/hotcoldcups/article/fit727x484/1/%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D9%83%D9%88%D9%83%D8%AA%D9%8A%D9%84-%D8%A7%D9%84%D8%A8%D8%B1%D8%AA%D9%82%D8%A7%D9%84-%D9%88%D8%A7%D9%84%D9%83%D9%8A%D9%88%D9%8A.jpeg",
        price: [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghaly_diet",
        details: "كيوى - تفاح - جرجير"
    },{
        key: 154,
        titleEn: "dish 1",
        titleAr: "فيتنس",
        image: "https://cuisine.nessma.tv/uploads/1/2019-08/299ec0e99fdbd6dd9b5c6cf7fb20f24f.jpg",
        price: [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghaly_diet",
        details: "كيوى - جنزبيل - ليمون"
    },{
        key: 155,
        titleEn: "dish 1",
        titleAr: "جديد فرغلى",
        image: "https://www.annahar.com/ContentFilesArchive/348838Image1-1180x677_d.jpg?version=2233672",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_diet",
        details: "تفاح اخضر - اناناس - برتقال"
    },{
        key: 156,
        titleEn: "dish 1",
        titleAr: "عود فرنساوى",
        image: "https://media.gemini.media/img/large/2018/5/8/2018_5_8_14_52_5_54.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_diet",
        details: "تفاح اخضر - اناناس - خوخ"
    },{
        key: 157,
        titleEn: "dish 1",
        titleAr: "رانى",
        image: "https://shamlola.s3.amazonaws.com/Shamlola_Images/8/src/7913dcfdf6d270d70d28c42b00a044b6155882a1.jpg",
        price: [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "farghaly_diet",
        details: "تفاح - موز - برتقال"
    },{
        key: 158,
        titleEn: "dish 1",
        titleAr: "جرين",
        image: "https://images.akhbarelyom.com/images/images/large/20210905120616116.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "farghaly_diet",
        details: "كيوى - تفاح اخضر - موز"
    },{
        key: 159,
        titleEn: "dish 1",
        titleAr: "كوكتيل ساده وسط",
        image: "https://rwa2ej.com/wp-content/uploads/2022/08/%D9%83%D9%88%D9%83%D8%AA%D9%8A%D9%84-%D8%A7%D9%84%D9%81%D9%88%D8%A7%D9%83%D8%A9-1200x900.jpg",
        price: 30,
        category: "salad_fruit",
        details: ""
    },{
        key: 160,
        titleEn: "dish 1",
        titleAr: "كوكتيل سادة وسط بالايس",
        image: "https://i.ytimg.com/vi/20aRDo-8Ff4/maxresdefault.jpg",
        price: 35,
        category: "salad_fruit",
        details: ""
    },{
        key: 161,
        titleEn: "dish 1",
        titleAr: "علبة فروت سلاط سادة",
        image: "https://img.youm7.com/ArticleImgs/2017/7/4/74869-%D8%B7%D8%A8%D9%82-%D8%A7%D9%84%D9%81%D8%B1%D9%88%D8%AA-%D8%B3%D9%84%D8%A7%D8%B7.jpg",
        price: 30,
        category: "salad_fruit",
        details: ""
    },{
        key: 162,
        titleEn: "dish 1",
        titleAr: "علبة فروت سلاط بالايس",
        image: "https://www.algamal.net/UserFiles/News/2020/08/09/51334.jpg",
        price: 40,
        category: "salad_fruit",
        details: ""
    },{
        key: 163,
        titleEn: "dish 1",
        titleAr: "فروت + ايس + بوريو",
        image: "https://i.ytimg.com/vi/oBc4UaS0F7I/maxresdefault.jpg",
        price: 45,
        category: "salad_fruit",
        details: ""
    },{
        key: 164,
        titleEn: "dish 1",
        titleAr: "قنبله فرغلى",
        image: "https://s3-eu-west-1.amazonaws.com/elmenusv5-stg/Normal/31c9b19b-3c24-4c86-9182-d1d72c5d6a0c.jpg",
        price: 50,
        category: "salad_fruit",
        details: ""
    },{
        key: 165,
        titleEn: "dish 1",
        titleAr: "سوبر فنبله فرغلى",
        image: "https://s3-eu-west-1.amazonaws.com/elmenusv5-stg/Normal/50bb9b38-7f1b-412a-ab4f-9d0c8909b3fa.jpg",
        price: 60,
        category: "salad_fruit",
        details: ""
    },{
        key: 166,
        titleEn: "dish 1",
        titleAr: "ايس كريم امريكي",
        image: "https://images.akhbarelyom.com/images/images/large/20220327113712343.jpg",
        price: 55,
        category: "salad_fruit",
        details: ""
    },{
        key: 167,
        titleEn: "dish 1",
        titleAr: "ايس كريم بالكاجو",
        image: "https://kalamelnasnew.com/wp-content/uploads/2019/11/dolci-vegani.jpg",
        price: 65,
        category: "salad_fruit",
        details: ""
    },{
        key: 168,
        titleEn: "dish 1",
        titleAr: "ايس كريم فسدق",
        image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2rrKgtNcJ5hNH1qN9vv68sa_r3KsaJ9cj0XsRZ3VIrEzuvDwvcwPfg9FeUiMNn6WFXAE&usqp=CAU",
        price: 65,
        category: "salad_fruit",
        details: ""
    },{
        key: 169,
        titleEn: "dish 1",
        titleAr: "ايس كريم بندق",
        image: "https://1.bp.blogspot.com/-x-Q8QjjE8Nk/XPSNd15LpAI/AAAAAAAAHcg/g2xOCu4vPSkAPz222OBngLXu3CfwWHKmgCLcBGAs/s1600/roasted-hazelnut-vanilla-ice-cream-articleLarge.jpg",
        price: 65,
        category: "salad_fruit",
        details: "" 
    },{
        key: 170,
        titleEn: "dish 1",
        titleAr: "ايس كريم لوز",
        image: "https://shamlola.s3.amazonaws.com/Shamlola_Images/3/src/4ee1f554ca3aa1d0fca37be73e29f2dee8faeb2e.jpg",
        price: 65,
        category: "salad_fruit",
        details: ""
    },{
        key: 171,
        titleEn: "dish 1",
        titleAr: "فتة ايس كريم",
        image: "https://i.ytimg.com/vi/ItwZx2x2q74/maxresdefault.jpg",
        price: 45,
        category: "salad_fruit",
        details: ""
    },{
        key: 172,
        titleEn: "dish 1",
        titleAr: "موز اسبيشيال",
        image: "https://img.youm7.com/large/720153113303130.jpg",
        price: 45,
        category: "salad_fruit",
        details: ""
    },{
        key: 173,
        titleEn: "dish 1",
        titleAr: "سوبر بوريو",
        image: "https://images.akhbarelyom.com/images/images/large/20190627111750369.jpg",
        price: 55,
        category: "salad_fruit",
        details: ""
    },{
        key: 174,
        titleEn: "dish 1",
        titleAr: "سوبر اوريو",
        image: "https://i.ytimg.com/vi/HzeaGG3RMyw/maxresdefault.jpg",
        price: 55,
        category: "salad_fruit",
        details: ""
    },{
        key: 175,
        titleEn: "dish 1",
        titleAr: "سوبر هوهوز",
        image: "https://i.ytimg.com/vi/vy6VfQZ1KaI/maxresdefault.jpg",
        price: 55,
        category: "salad_fruit",
        details: ""
    },{
        key: 176,
        titleEn: "dish 1",
        titleAr: "سوبر توينكز",
        image: "https://almelnoujoum.com/wp-content/uploads/2022/07/inbound6151566818023821786.jpg",
        price: 55,
        category: "salad_fruit",
        details: ""
    },{
        key: 177,
        titleEn: "dish 1",
        titleAr: "كيوى قطع بالايس",
        image: "https://www.almrsal.com/wp-content/uploads/2013/10/579085_585422068187069_441195014_n.jpg",
        price: 60,
        category: "salad_fruit",
        details: ""
    },{
        key: 178,
        titleEn: "dish 1",
        titleAr: "عرايس ",
        image: "https://modo3.com/thumbs/fit630x300/180862/1514714464/%D8%B7%D8%B1%D9%8A%D9%82%D8%A9_%D8%B9%D9%85%D9%84_%D8%AA%D8%B1%D8%A7%D9%8A%D9%81%D9%84_%D8%A7%D9%84%D9%85%D8%A7%D9%86%D8%AC%D9%88.jpg",
        price: 60,
        category: "salad_fruit",
        details: "مانجو قطع + قشطه + عسل + ايس + مكسرات"
    },{
        key: 179,
        titleEn: "dish 1",
        titleAr: "اناناس قطع بالايس",
        image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdY9H5W70p1myNY04LsKb1CtvqE2RhCV4hojT6ao1fs_2FK-suyjCZUk2RQoYCTQRX3_M&usqp=CAU",
        price: 70,
        category: "salad_fruit",
        details: ""
    },{
        key: 180, 
        titleEn: "dish 1",
        titleAr: "رمان حب + ايس",
        image: "https://www.supermama.me/system/App/Models/Recipe/images/000/049/671/watermarked/SM31200.jpg",
        price: 40,
        category: "salad_fruit",
        details: ""
    },{
        key: 181,
        titleEn: "dish 1",
        titleAr: "مانجو قطع بالايس",
        image: "https://www.arabfive.news/wp-content/uploads/2022/09/%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%A2%D9%8A%D8%B3-%D9%83%D8%B1%D9%8A%D9%85-%D9%85%D8%A7%D9%86%D8%AC%D9%88.jpg",
        price: 50,
        category: "salad_fruit",
        details: ""
    },{
        key: 182,
        titleEn: "dish 1",
        titleAr: "بطيخ قطع بالايس",
        image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYPJsXcau8_z2d6YrIjEhB40ci5vp5MU4tbtwa01qLlpZoegVX3pzh-SAqfAUptRWcb2k&usqp=CAU",
        price: 40,
        category: "salad_fruit",
        details: ""
    },{
        key: 183,
        titleEn: "dish 1",
        titleAr: "التوربيني",
        image: "https://images.akhbarelyom.com/images/images/large/20190627111750369.jpg",
        price: 60,
        category: "salad_fruit",
        details: "بوريو - اوريو - هوهوز - توينكز - ايس كريم"
    },{
        key: 184,
        titleEn: "dish 1",
        titleAr: "اناناس قطع",
        image: "https://www.almrsal.com/wp-content/uploads/2016/07/%D9%83%D9%8A%D9%81-%D8%AA%D9%82%D9%88%D9%85-%D8%A8%D8%AA%D9%82%D8%B4%D9%8A%D8%B1-%D9%88-%D8%AA%D9%82%D8%B7%D9%8A%D8%B9-%D8%A7%D9%84%D8%A7%D9%86%D8%A7%D9%86%D8%A7%D8%B3.jpg",
        price: 65,
        category: "salad_fruit",
        details: ""
    },{
        key: 185,
        titleEn: "dish 1",
        titleAr: "الديناصور",
        image: "https://img.youm7.com/ArticleImgs/2018/5/10/48711-%D8%B1%D8%B2-%D8%A8%D9%84%D8%A8%D9%86-%D8%A8%D8%A7%D9%84%D9%83%D9%86%D8%A7%D9%81%D8%A9.jpg",
        price: 60,
        category: "salad_fruit",
        details: "ارز باللبن + كنافه بالقشطه + مانجو قطع + ايس + مكسرات"
    },{
        key: 186,
        titleEn: "dish 1",
        titleAr: "خوخ ايس",
        image: "https://www.thaqfny.com/wp-content/uploads/2021/06/%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%A2%D9%8A%D8%B3-%D9%83%D8%B1%D9%8A%D9%85-%D8%A7%D9%84%D8%AE%D9%88%D8%AE.jpg",
        price: 55,
        category: "salad_fruit",
        details: ""
    },{
        key: 187,
        titleEn: "dish 1",
        titleAr: "برقوق قطع بالايس",
        image: "https://www.lazezh.com/wp-content/uploads/gcMigration/recipes/1/1e221bd3c18e2af.jpg",
        price: 60,
        category: "salad_fruit",
        details: ""
    },{
        key: 188,
        titleEn: "dish 1",
        titleAr: "رمان حب",
        image: "https://modo3.com/thumbs/fit630x300/174136/1493901378/%D8%B7%D8%B1%D9%8A%D9%82%D8%A9_%D8%A7%D8%B3%D8%AA%D8%AE%D8%B1%D8%A7%D8%AC_%D8%AD%D8%A8_%D8%A7%D9%84%D8%B1%D9%85%D8%A7%D9%86.jpg",
        price: 30,
        category: "salad_fruit",
        details: ""
    },{
        key: 189,
        titleEn: "dish 1",
        titleAr: "البرازيلى",
        image: "https://yummy.awicdn.com/site-images/sites/default/files/prod/recipe/5/a/466453/133c494f34e3f56fa6fae00c4ededa2ce11e77e7-300322080641.jpg?preset=v3.0_DYNxDYN&save-png=1&rnd=1519151RND220215",
        price: [{price: 35, type: "وسط"},{price: 45, type: "كبير"}],
        category: "salad_fruit",
        details: "ليمون - نعناع - سفن"
    },{
        key: 190,
        titleEn: "dish 1",
        titleAr: "لوتس",
        image: "https://www.foodtodayeg.com/Content/Upload/large/32023251661701154149.jpg",
        price: [{price: 45, type: "وسط"},{price: 50, type: "كبير"}],
        category: "salad_fruit",
        details: ""
    },{
        key: 191,
        titleEn: "dish 1",
        titleAr: "كيوى قطع ساده",
        image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKx6cAKRX3M4RWvnkExIUQfLxCOVTDFOdJ1rqS9mLfcgIo99ZFReu2tfZKWfHMPk5Aj6I&usqp=CAU",
        price: 50,
        category: "salad_fruit",
        details: ""
    },{
        key: 192,
        titleEn: "dish 1",
        titleAr: "الكبير اوى اوى",
        image: "https://www.almrsal.com/wp-content/uploads/2013/10/579085_585422068187069_441195014_n.jpg",
        price: 60,
        category: "salad_fruit",
        details: "افوكادو - كيوى - بلح - تفاح - عسل - مكسرات - ايس كريم"
    },{
        key: 193,
        titleEn: "dish 1",
        titleAr: "ارز بلبن",
        image: "https://images.akhbarelyom.com/UP/20221003145208811.jpg",
        price: [{price: 30, type: "وسط"},{price: 35, type: "كبير"}],
        category: "rice_milk",
        details: ""
    },{
        key: 194,
        titleEn: "dish 1",
        titleAr: "ارز بلبن بالمكسرات",
        image: "https://www.thaqfny.com/wp-content/uploads/2021/03/%D8%B7%D8%B1%D9%8A%D9%82%D8%A9-%D8%B9%D9%85%D9%84-%D8%A7%D9%84%D8%A7%D8%B1%D8%B2-%D8%A8%D8%A7%D9%84%D9%84%D8%A8%D9%86-%D8%A7%D9%84%D9%85%D8%A7%D9%84%D9%83%D9%89-2.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "rice_milk",
        details: ""
    },{
        key: 195,
        titleEn: "dish 1",
        titleAr: "ارز بلبن بالمانجو",
        image: "https://img.youm7.com/ArticleImgs/2017/8/1/35310-%D8%A7%D9%84%D8%A3%D8%B1%D8%B2-%D8%A8%D9%84%D8%A8%D9%86-%D8%A8%D8%A7%D9%84%D9%85%D8%A7%D9%86%D8%AC%D9%88.jpeg",
        price: [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "rice_milk",
        details: ""
    },{
        key: 196,
        titleEn: "dish 1",
        titleAr: "ارز بلبن بالايس كريم",
        image: "https://i.ytimg.com/vi/LKXkF5evZfw/maxresdefault.jpg",
        price: [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "rice_milk",
        details: ""
    },{
        key: 197,
        titleEn: "dish 1",
        titleAr: "ارز بلبن باللوتس",
        image: "https://www.masr-online.com/wp-content/uploads/2022/08/WhatsApp-Image-2022-08-15-at-9.41.04-AM-780x470.jpeg",
        price: [{price: 35, type: "وسط"},{price: 40, type: "كبير"}],
        category: "rice_milk",
        details: ""
    },{
        key: 198,
        titleEn: "dish 1",
        titleAr: "ارز بلبن بالنوتيلا",
        image: "https://images.deliveryhero.io/image/talabat/Menuitems/super_nutella_rice_p_637435831444799519.jpg",
        price: [{price: 40, type: "وسط"},{price: 45, type: "كبير"}],
        category: "rice_milk",
        details: ""
    },
]


export default DishesData